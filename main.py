import pygame
import random
import time


size = width, height = 800, 600
screen = None


class Ball:
    def __init__(self, x, y, x_speed, y_speed):
        self.x = x
        self.y = y
        self.x_speed = x_speed
        self.y_speed = y_speed
        self.img = pygame.image.load('basketball.png')
        self.rect = self.img.get_rect()
        self.rect.x = width // 2 - self.rect.width // 2
        self.rect.y = height // 2 - self.rect.height // 2
        self.width = self.rect.width
        self.height = self.rect.height

    def draw(self):
        self.rect.x = self.x
        self.rect.y = self.y
        screen.blit(self.img, self.rect)

    def handle_collision(self, p):
        is_collided_with_terrain = not(0 < self.x + self.x_speed < width - self.width and 0 < self.y + self.y_speed < height - self.height)
        is_collided_with_platform_x = not(self.x + self.x_speed < p.x - self.width or self.x + self.x_speed > p.x + p.width)
        is_collided_with_platform_y = not(self.y + self.y_speed < p.y - self.height or self.y + self.y_speed > p.y + p.height)
        is_collided = is_collided_with_platform_x and is_collided_with_platform_y or is_collided_with_terrain

        if not(is_collided):
            self.x += self.x_speed
            self.y += self.y_speed
        else:
            if is_collided_with_terrain:
                if self.x + self.width + self.x_speed >= width:
                    self.x = width - self.width
                    self.x_speed *= -1
                if self.x + self.x_speed <= 0:
                    self.x = 0
                    self.x_speed *= -1
                if self.y + self.height + self.y_speed >= height:
                    self.y = height - self.height
                    self.y_speed *= -1
                if self.y + self.y_speed <= 0:
                    self.y = 0
                    self.y_speed *= -1
            else:
                self.x_speed *= -1
                self.y_speed *= -1


class Platform:
    def __init__(self, pl_width=200, pl_height=20, pl_color=(0, 255, 0)):
        self.width = pl_width
        self.height = pl_height
        self.color = pl_color
        self.x = width // 2 - self.width // 2
        self.bottom_margin = 30
        self.y = height - self.bottom_margin - self.height
        self.is_moving = False
        self.moving_direction = 0

    def move(self, delta_x, delta_y):
        self.x += delta_x
        self.y += delta_y
        if self.y + self.height > height:
            self.y = height - self.height
        if self.x + self.width > width:
            self.x = width - self.width

    def draw(self):
        global screen
        rect = (self.x, self.y, self.width, self.height)
        pygame.draw.rect(screen, self.color, rect, 0)

    def handle_key_down(self, key):
        if key == pygame.K_a:
            self.moving_direction = 1
            self.is_moving = True
        if key == pygame.K_d:
            self.moving_direction = 2
            self.is_moving = True

    def handle_key_up(self, key):
        self.is_moving = False

    def handle_moving(self):
        if self.is_moving:
            if self.moving_direction == 1:
                self.move(-5, 0)
            else:
                self.move(5, 0)


background = None
ball_x_coordinate = width // 2
ball_y_coordinate = height // 2
gameover = False
speed = 10
wait_time = 0.01
b = Ball(width // 2, height // 2, 0, 0)
p = Platform()


def initialize():
    global screen, background
    pygame.init()
    background = (0, 0, 0)
    screen = pygame.display.set_mode(size)


def start_simulation():
    global b
    b.x_speed = random.randint(-speed, speed)
    b.y_speed = int((speed ** 2 - b.x_speed ** 2) ** 0.5)
    screen.fill(background)
    b.draw()
    pygame.display.flip()


def handle_events():
    global b, gameover

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameover = True
        if event.type == pygame.KEYDOWN:
            p.handle_key_down(event.key)
        if event.type == pygame.KEYUP:
            p.handle_key_up(event.key)

    b.handle_collision(p)
    p.handle_moving()
    b.draw()
    p.draw()
    time.sleep(wait_time)


def main():
    global screen
    initialize()
    start_simulation()
    while not gameover:
        screen.fill(background)
        handle_events()
        pygame.display.flip()


main()